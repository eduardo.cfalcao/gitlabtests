﻿using Microsoft.AspNetCore.Mvc;

namespace GitLab.Tests.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HealthCheckController : ControllerBase
    {
        private const string VERSION = "1.0";

        [HttpGet]
        public HelthCheck HealthCheck() => new HelthCheck
        {
            IsHealth = true,
            NodeName = System.Environment.MachineName,
            Version = VERSION
        };
    }

    public class HelthCheck
    {
        public bool IsHealth { get; set; }

        public string NodeName { get; set; }

        public string Version { get; set; }
    }
}
